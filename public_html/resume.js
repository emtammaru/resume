/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */  
        
console.log(ko);

var myViewModel = {
    
    randomColor:function(){return "#"+((1<<24)*Math.random()|0).toString(16)},
    nameInfo: 'My grand-dad is from Estonia, which is where this name originates! -- pronounced like kangaroo. . .',
    personAge: 24,
    phoneInfo: 'This is my personal cell.  Text, Snapchat, WhatsApp, or Yo me! ',
    addressInfo: 'I live in Point Loma. ',
    emailInfo: 'I wanted the standard first initial + last name gmail address, but my Dad beat me to it. . .',
    eduInfo: '  With a GPA of 3.97, I was awarded membership in Phi Delta Lambda, a Baccalaureate Scholarship Honor Society. ',
    /// 'visible' bindings 
    poiBits: {
        'nameInfo':ko.observable(false),
        'phoneInfo':ko.observable(false),
        'addressInfo':ko.observable(false),
        'emailInfo':ko.observable(false),
        'eduInfo':ko.observable(true),
        'edusInfos':ko.observable(true),
        'diploma':ko.observable(false),
        'jobexpInfos':ko.observable(true),
        'jobexpInfo1':ko.observable(true),
    },
    /// toggling the 'point of interest' (poi)
    poiClick: function(poi){
        
        var self = this;
        
        if(poi==='nameInfo'){     
            var $content = $('#name-plug');
            if(!self.poiBits[poi]()){
                
                self.poiBits[poi](true);
                $content.animate({
                    "margin-left": -1035
                }, 200);
            }
            else{
                $content.animate({
                    "margin-left": -2000
                }, 100, 'swing', function(){
                    self.poiBits[poi](false);
                });
            }
        }else{
        
            if(self.poiBits[poi]())
                self.poiBits[poi](false);
            else
                self.poiBits[poi](true);
        }
        
    },
    emailClick: function(){
        
        var $content = $('#email-drawer');

        $content.animate({
            "margin-top": 0
        }, 200);
        
    },
    phoneClick: function(){
        
        var $content = $('#phone-drawer');

        $content.animate({
            "margin-left": 0
        }, 200);
        
    },
    leftHandleHtml:''+
                '<div class="poi-minimize-left-handle"  >'+
                 '   <div class="poi-minimize-left-handle-bar-top" ></div>'+
                  '  <div class="poi-minimize-left-handle-bar-bottom" ></div>'+
                '</div>'+
                '<div class="poi-minimize-left-handle-bar-grab-container" >'+
                '    <div class="poi-minimize-left-handle-bar-grab"></div>'+
                '</div>',
    upHandleHtml:''+
        '<div class="poi-minimize-top-handle"  >'+
         '   <div class="poi-minimize-top-handle-bar-top" ></div>'+
          '  <div class="poi-minimize-top-handle-bar-bottom" ></div>'+
        '</div>'+
        '<div class="poi-minimize-top-handle-bar-grab-container" >'+
        '    <div class="poi-minimize-top-handle-bar-grab"></div>'+
        '</div>',
    emailHandleHtml:''+
        '<div id="email-minimize-top-handle"  >'+
         '   <div class="poi-minimize-top-handle-bar-top" ></div>'+
          '  <div class="poi-minimize-top-handle-bar-bottom" ></div>'+
        '</div>'+
        '<div id="email-minimize-top-handle-bar-grab-container" >'+
        '    <div class="poi-minimize-top-handle-bar-grab"></div>'+
        '</div>',
    rightHandleHtml:''+
                '<div class="poi-minimize-right-handle"  >'+
                 '   <div class="poi-minimize-right-handle-bar-top" ></div>'+
                  '  <div class="poi-minimize-right-handle-bar-bottom" ></div>'+
                '</div>'+
                '<div class="poi-minimize-right-handle-bar-grab-container" >'+
                '    <div class="poi-minimize-right-handle-bar-grab">'+
                '       <div class="poi-minimize-right-handle-bar-grab poi-minimize-right-handle-bar-grab-inner-hemi" ></div>'+
                '    </div>'+
                '</div>'+
                '<div class="left-wire">'+
//                '   <div class="left-wire-piece"></div>'+
                '</div>'
                
};

ko.applyBindings(myViewModel);
